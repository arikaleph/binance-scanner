# binance-scanner

A script checking Binance announcements in five-minute-intervals and 
sending Twilio notifications.

## Installation

First, install all Node dependencies.

```shell
npm install
```

Second, generate the Javascript files from the Typescript source.

```shell
npm run tsc
```

## Setup (Optional)

To view the debug log, set the `DEBUG` environment variable to `announcement-scanner:*`.

```shell
export DEBUG=announcement-scanner:*
```

## Run

Run the monitoring script.

```shell
node ./src/scan_binance_announcements.js
```

## Heroku

### Deploy to Heroku

To deploy, simply push the repository to the Heroku remote's master branch.

```shell
git push heroku master
```

### Start the Background Process

The script execution process name is `binance_observer`, defined in the Procfile.

```shell
heroku ps:scale binance_observer=1
```

Given this project doesn't have or need a web process, you could also run

```shell
heroku ps:scale web=0
```

These commands can also (optionally) be combined:

```shell
heroku ps:scale web=0 binance_observer=1
```

### Resize the Background Process

By default, Heroku shuts down free instances after a few minutes of inactivity. 
To avoid such a shutdown, upgrade to a paid plan and resize the background
process from a free tier to the hobby tier.

```shell
heroku ps:resize binance_observer=hobby
```

### Logging

To enable logging on Heroku, add the `DEBUG` config var to the Heroku environment.

```shell
heroku config:set DEBUG=announcement-scanner:*
```

To tail the log of the `binance_observer` process, simply run

```shell
heroku logs --tail --dyno binance_observer
```