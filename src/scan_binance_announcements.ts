import cheerio from 'cheerio';
import * as debugModule from 'debug';
import fetch from 'node-fetch';
import twilio from 'twilio';

const debug = debugModule.default('announcement-scanner:binance');

const accountSid = process.env.TWILIO_ACCOUNT_SID || 'EDITME'; // EDIT ME
const authToken = process.env.TWILIO_AUTH_TOKEN || 'EDITME'; // EDIT ME
const TwilioClient = twilio(accountSid, authToken);

const TWILIO_SENDER_PHONE = process.env.TWILIO_SENDER_PHONE || '+1…'; // EDIT ME

const ERROR_RECIPIENT = process.env.ERROR_RECIPIENT || '+1…'; // EDIT ME
const SUCCESS_RECIPIENTS = [
	ERROR_RECIPIENT,
	// OPTIONALLY ADD OTHERS
];

const BROADCAST_INITIAL_ANNOUNCEMENTS = true; // FLIP ME FOR DEBUGGING

const SCHEDULING_URL = 'https://www.binance.com/en/amp/support/announcement/c-48';
const SCAN_INTERVAL_NO_SLOTS = 5 * 1000 * 60; // only check every 5 minutes
const SCAN_INTERVAL_ERROR = 1000 * 65; // check back in a minute and five seconds

const announcementTitlesByUrl = {};
const knownAnnouncementUrls = new Set();

function delay(ms) {
	return new Promise(resolve => setTimeout(resolve, ms));
}

async function getRawHTML() {
	const htmlResponse = await fetch(SCHEDULING_URL);
	const html = await htmlResponse.text();
	return html;
}

async function parseBinanceAnnouncements() {
	let html;
	try {
		html = await getRawHTML();
	} catch (e) {
		debug('HTML fetching error: %s', e.message);
		notifyAboutError(e);

		debug('Waiting %d minutes: %s', SCAN_INTERVAL_ERROR / 60000, new Date().toLocaleString());
		await delay(SCAN_INTERVAL_ERROR);
		return parseBinanceAnnouncements();
	}

	// @ts-ignore
	const announcementLinks = cheerio.default('div.css-vurnku > a[data-bn-type=link]', html);

	const isFirstScan = (knownAnnouncementUrls.size === 0);
	const mayBroadcast = (!isFirstScan || BROADCAST_INITIAL_ANNOUNCEMENTS);

	const currentAnnouncementUrls = new Set<string>();
	const currentAnnouncementDetails = [];
	const newAnnouncements = new Set<string>();
	announcementLinks.each((index, element) => {
		// @ts-ignore
		const currentUrl = element.attribs['href'];
		// @ts-ignore
		const currentText = cheerio.default(element).contents().text().trim();
		announcementTitlesByUrl[currentUrl] = currentText;
		currentAnnouncementUrls.add(currentUrl);
		
		currentAnnouncementDetails.push(`${currentText} (https://www.binance.com${currentUrl})`)
		
		if (!knownAnnouncementUrls.has(currentUrl)) {
			// this is a new one
			if (!isFirstScan) {
				newAnnouncements.add(currentUrl);
			} else if (BROADCAST_INITIAL_ANNOUNCEMENTS && newAnnouncements.size === 0) {
				// if we're broadcasting the initial announcements, we're only broadcasting one
				newAnnouncements.add(currentUrl);
			}
		}
		knownAnnouncementUrls.add(currentUrl);
	});

	debug('Currently shown announcements: %O', currentAnnouncementDetails);

	const hasNewAnnouncements = (newAnnouncements.size > 0);

	// when should we broadcast?
	if (mayBroadcast && hasNewAnnouncements) {
		debug('Broadcasting change!');
		const newAnnouncementArray = Array.from(newAnnouncements);
		await broadcastAnnouncements(newAnnouncementArray);
	}

	const scanInterval = SCAN_INTERVAL_NO_SLOTS;

	debug('Waiting %d minutes: %s', scanInterval / 60000, new Date().toLocaleString());
	await delay(scanInterval);
	return parseBinanceAnnouncements();
}

async function notifyAboutError(error: Error): Promise<void> {
	const message = `Binance tracker error:\n\n${error.message}`;
	const twilioOptions = {
		from: TWILIO_SENDER_PHONE,
		to: ERROR_RECIPIENT
	};
	await TwilioClient.messages.create({
		...twilioOptions,
		body: message
	});
}

async function broadcastAnnouncements(urls: string[]) {
	let locationTexts = [];
	for (const currentUrl of urls) {
		const currentTitle = announcementTitlesByUrl[currentUrl];
		// locationTexts.push(`\n\n${currentTitle}:\n${currentUrl}`);
		locationTexts.push(`\n\n${currentTitle}`);
	}
	const announcementTexts = locationTexts.join('');
	const message = `‼️ New Binance announcements! ‼️${announcementTexts}\n${SCHEDULING_URL}`;

	const recipients = SUCCESS_RECIPIENTS;
	const promises = [];

	for (const currentRecipient of recipients) {
		const twilioOptions = {
			from: TWILIO_SENDER_PHONE,
			to: currentRecipient
		};

		const promise = TwilioClient.messages.create({
			...twilioOptions,
			body: message
		});
		promises.push(promise);
	}

	await Promise.all(promises);
}

(async () => {
	await parseBinanceAnnouncements();
})();
